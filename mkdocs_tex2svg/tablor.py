import giacpy as g
from giacpy import giac, assume, pi, derive, factor, simplify, solve, tabvar, domain, subst

import subprocess
import shlex

X = 0
FPX = 1
FX = 2
FPPX = F2X = 3

SPACE = '" "'
PLUS_SIGN = '"+"'
MINUS_SIGN = '"-"'
PLUS_INFINITY = "+infinity"
MINUS_INFINITY = "-infinity"
DOUBLE_BAR = '"||"'
HIDDEN_SIGN = '"h"'

UP_ARROW = '"↑"'
DOWN_ARROW = '"↓"'
WHAT = '"- (∩)"'
ELSE = '"- (∪)"'

print("HELLO FROM INSIDE")

# a, x,y,z = g.giac('a, x,y,z')
# g.assume('x>-pi && x<pi')
# l = g.solve('x^2-1=0',x)

def get_options(content):
    assert "{" in content and "}" in content, "Tablor Options must be a Python dictionnary"
    i0 = content.find("{") # returns -1 if not found
    i1 = content.rfind("}") # returns -1 i fnot found
    i2 = content.find("=") # returns -1 if not found
    assert i2 < i0 and content[:i2].strip(" ") == "options", f"""keyword : {content[:i2].strip(" ")} is not recognised. Use 'options={{key:value}} pairs syntax"""
    s = content[i0+1:i1].strip(" ")
    d = {}
    for el in s.split("\n"):
        if el.strip(" ") != "" and ":" in el:
            key, value = el.split(":")
            key = key.strip(" ")
            value = value.strip(" ")
            if value == "true":
                value = "True"
            elif value == "false":
                value = "False"
            d[key] = eval(value)
    return d

def remove_fpx_values(xValues, fpxValues, fxValues):
    print("XVALUES", xValues)
    print("FPXVALUES", fpxValues)
    print("FXVALUES", fxValues)
    newXValues = []
    newFpxValues = []
    newFxValues = []
    i = 0
    while i <= len(fpxValues)-1:
        print("i=", i)
        print("fpxValues[i]=", fpxValues[i])
        if fpxValues[i] == "0":
            newXValues.append(simplify(rf"{xValues[int(i//2)]}"))
            newFpxValues.append(simplify(rf"{fpxValues[i]}"))
            newFxValues.append(simplify(rf"{fxValues[i]}"))
            i += 1
        elif fpxValues[i] == PLUS_INFINITY or fpxValues[i] == MINUS_INFINITY:
            newXValues.append(simplify(rf"{xValues[int(i//2)]}"))
            newFpxValues.append(None)
            if i <= len(fxValues)-1:
                newFxValues.append(simplify(rf"{fxValues[i]}"))
            i += 1
                # i += 1
        elif fpxValues[i] == PLUS_SIGN:
            if i <= len(fpxValues)-3 and not fpxValues[i+1] == "0" and fpxValues[i+2] == PLUS_SIGN:
                newFpxValues.append(simplify(rf"{fpxValues[i]}"))
                newFxValues.append(simplify(rf"{fxValues[i]}"))
                i += 3
            else:
                newFpxValues.append(simplify(rf"{fpxValues[i]}"))
                newFxValues.append(simplify(rf"{fxValues[i]}"))
                i += 1
        elif fpxValues[i] == MINUS_SIGN:
            if i <= len(fpxValues)-3 and not fpxValues[i+1] == "0" and fpxValues[i+2] == MINUS_SIGN:
                newFpxValues.append(simplify(rf"{fpxValues[i]}"))
                newFxValues.append(simplify(rf"{fxValues[i]}"))
                i += 3
            else:
                newFpxValues.append(simplify(rf"{fpxValues[i]}"))
                newFxValues.append(simplify(rf"{fxValues[i]}"))
                i += 1
        else:
        #     newXValues.append(simplify(rf"{xValues[int(i//2)]}"))
        #     newFpxValues.append(simplify(rf"{fpxValues[i]}"))
        #     newFxValues.append(simplify(rf"{fxValues[i]}"))
            i += 1

    return newXValues, newFpxValues, newFxValues

def get_domain(fx:str, x:str):
    return domain(fx, x)

def get_fx(fx:str, xName:str, xValue):
    return subst(fx, f"{xName}={xValue}")

def est_fpaire(fx, x):
    minusX = rf"-{x}"
    return simplify(get_fx(fx,x, x) - get_fx(fx, x, minusX)) == 0

def est_fimpaire(fx, x):
    minusX = rf"-{x}"
    return simplify(get_fx(fx, x, x) + get_fx(fx, x, minusX)) == 0

def extend_xValuesBySymetry(l):
    "extends list l when fx is an even function"
    newRight = l
    newLeft = get_XSymetry(l)
    newL = newLeft+newRight
    return newL

def get_XSymetry(l):
    "If l=[0,1, +infinity], returns [-infinity, -1]" 
    l = l[1:]
    l.reverse()
    newL = []
    for value in l: # deals with both infinities and values
        newL.append(simplify(rf"-{value}"))
    return newL

def extend_fxPaireValues(l):
    "extends list l when fx is an even function"
    newRight = l
    newLeft = get_fxPaireSymetry(l)
    # newL = newLeft+newRight
    newL = newLeft
    for value in newRight:
        newL.append(value)
    return newL

def get_fxPaireSymetry(l):
    "If l=[0,1, +infinity], returns [-infinity, -1]" 
    l = l[1:]
    l.reverse()
    newL = []
    for value in l: # deals with both infinities and values
        if value == UP_ARROW:
            newL.append(simplify(DOWN_ARROW))
        elif value == DOWN_ARROW:
            newL.append(simplify(UP_ARROW))
        else:
            newL.append(simplify(rf"{value}"))
    newL.reverse()
    print("NEWL =", newL)
    return newL

def extend_fxImpaireValues(l):
    "extends list l when fx is an even function"
    newRight = l
    newLeft = get_fxImpaireSymetry(l)
    # this does not work :
    # newL = newLeft+newRight
    newL = newLeft
    for value in newRight:
        newL.append(value)
    return newL

def get_fxImpaireSymetry(l):
    "If l=[0,1, +infinity], returns [-infinity, -1]" 
    l = l[1:]
    l.reverse()
    print("l AVANT=", l)
    newL = []
    for value in l: # deals with both infinities and values
        if value == UP_ARROW:
            newL.append(simplify(UP_ARROW))
        elif value == DOWN_ARROW:
            newL.append(simplify(DOWN_ARROW))
        else:
            newL.append(simplify(rf"-{value}"))
    newL.reverse()
    return newL

def extend_fpxPaireValues(l):
    "extends list l when fx is an even function"
    newRight = l
    newLeft = get_fpxPaireSymetry(l)
    newL = newLeft+newRight
    return newL

def get_fpxPaireSymetry(l):
    "If l=[0,1, +infinity], returns [-infinity, -1]" 
    l = l[1:]
    l.reverse()
    newL = []
    for value in l:
        if value == PLUS_SIGN:
            newL.append(simplify(MINUS_SIGN))
        elif value == MINUS_SIGN:
            newL.append(simplify(PLUS_SIGN))
        else: # deals with both infinities, and giac fpx values
            newL.append(simplify(rf"-{value}"))
    return newL

def extend_fpxImpaireValues(l):
    "extends list l when fx is an even function"
    newRight = l
    newLeft = get_fpxImpaireSymetry(l)
    newL = newLeft+newRight
    return newL

def get_fpxImpaireSymetry(l):
    "If l=[0,1, +infinity], returns [-infinity, -1]" 
    l = l[1:]
    l.reverse()
    newL = []
    for value in l:
        # if value == PLUS_SIGN:
        #     newL.append(simplify(MINUS_SIGN))
        # elif value == MINUS_SIGN:
        #     newL.append(simplify(PLUS_SIGN))
        # else: # deals with both infinities, and giac fpx values
        newL.append(simplify(rf"{value}"))
    return newL

def get_derivee(f:str,x:str):
    return factor(simplify(derive(f,x)))

def get_name_variable(fx: str):
    """ returns function name 'f', and function variable 'x'
    from string like "f(x)=x^3"
    """
    i_po = fx.index("(")
    i_pf = fx.index(")")
    i_eq = fx.index("=")
    if i_po < i_eq and i_pf < i_eq:
        return fx[:i_po], fx[i_po+1:i_pf]
    else:
        return "f", "x"

def get_latex(s:str):
    """ Converts Giac Expression to LaTex,
    AND then to python str,
    AND removes leading and tailing quotes,
    to be TeX-ready to be directly inserted in VarTables
    """
    s = str(g.latex(s))[1:-1]
    s = s.replace(" ", "")
    # s = escape_Tex_for_Python(s)
    return s

def get_tabvar(fx:str, x:str):
    # g.assume(f"{x}>=-1 && {x}<=1")
    return tabvar(fx, x)

def get_listeVI(tabvar):
    VI = []
    for i in range(len(tabvar[FPX])):
        if tabvar[FPX][i] == DOUBLE_BAR:
            if not tabvar[X][i] in VI:
                VI.append(tabvar[X][i])
    return VI

def est_VI(candidatVI, tabvar):
    return candidatVI in get_listeVI(tabvar)

def escape_Tex_for_Python(s:str):
    # s.replace("{", r"{{")
    # s.replace("}", r"}}")
    # return s
    newS = r""
    for c in s:
        if c == "{":
            c = r"{{"
        if c == "}":
            c = r"}}"
        newS += c
    return newS

def get_function_body(fx: str):
    """ returns function body 'x^3'
    from string like "f(x)=x^3"
    """
    i_eq = fx.index("=")
    return fx[i_eq+1:]

def get_solve(eqIneq:str, x:str)->list:
    return solve(eqIneq, x)
    # return g.resoudre(eqIneq, x)

def get_icas(command:str, content):
    # Start of Popen
    cmd_parts = [f"""icas '{command}'"""]
    i = 0
    p = {}
    for cmd_part in cmd_parts:
        try:
            cmd_part = cmd_part.strip()
            if i == 0:
                p[0]=subprocess.Popen(shlex.split(cmd_part),stdin=subprocess.PIPE, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
            else:
                p[i]=subprocess.Popen(shlex.split(cmd_part),stdin=p[i-1].stdout, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
            i += 1
        except Exception as e:
            err = str(e) + ' : ' + str(cmd_part)
            return (
                '<pre>Error : ' + err + '</pre>'
                '<pre>' + content + '</pre>').split('\n')
    (output, err) = p[i-1].communicate()
    exit_code = p[0].wait()
    output = output.decode("utf-8")
    i_co = output.index("list[")
    i_cf = output.index("]")
    # str to list
    output = output[i_co+5:i_cf+1].split(",")
    # list to Pygen.list
    output = giac(output)    
    return output

def get_xValuesInInterval(fpx:str, x:str, a:str , b:str)->list:
    fpx = factor(simplify(fpx))
    x = giac(f"{x}")
    # print(f"{x}>={a} && {x}<{b}")
    assume(f"{x}>={a} && {x}<={b}")
    return solve(f"{fpx}*({x}-{a})*({x}-{b})=0", x)
    # return g.resoudre(eqIneq, x)

def get_factor(s:str):
    return factor(s)

################################################
##                  X VALUES
################################################

# def get_xValues_from(tabvar):
def get_xValues_from(fx:str, x:str):
    tabvar = get_tabvar(f"{fx}", x)
    l = []
    for i in range(1,len(tabvar[X])):
        if tabvar[X][i] != SPACE:
            l.append(tabvar[X][i])
    if est_fpaire(fx, x) or est_fimpaire(fx, x):
        l = extend_xValuesBySymetry(l)
    return l

def get_xValuesTkz(xValuesTex:list)->str:
    """prepares xValues for insertion into TkzTab VarTable
    """
    xValues = r""
    for value in xValuesTex:
        value = escape_Tex_for_Python(value)
        xValues += rf"""${value}$, """
    xValues = xValues.rstrip(" ")[:-1]
    xValues = rf"""{{{xValues}}}"""
    return xValues

################################################
##                  FPX VALUES
################################################

# def get_fpxValues_from(tabvar):
def get_fpxValues_from(fx:str, x:str):
    tabvar = get_tabvar(fx, x)
    l = []
    for i in range(1,len(tabvar[FPX])):
        fpxEl = tabvar[FPX][i]
        l.append(fpxEl)
    if est_fpaire(fx, x):
        l = extend_fpxPaireValues(l)
    elif est_fimpaire(fx, x):
        l = extend_fpxImpaireValues(l)
    return l

def get_fpxValuesTkz(fpxValues:list)->str:
    """prepares fpxValues for insertion into TkzTabLine VarTable
    """
    # print("fpxValues from INSIDE TkZ=", fpxValues)
    # print("type(fpxValues[0)] (-infinity) from INSIDE TkZ=", type(fpxValues[0]))
    fpxValuesTkz = r""
    lastAddedWasDoubleBar = False
    for value in fpxValues:
        # value = escape_Tex_for_Python(value)
        if value == 0:
            # print("Adding 0 Detected..")
            fpxValuesTkz += r"z, "
            lastAddedWasDoubleBar = False
        elif value == None:
            fpxValuesTkz += r", "
            lastAddedWasDoubleBar = False
        elif value == PLUS_SIGN:
            # print("Adding Plus Sign Detected..")
            fpxValuesTkz += r"+, "
            lastAddedWasDoubleBar = False
        elif value == MINUS_SIGN:
            # print("Adding Minus Sign Detected..")
            fpxValuesTkz += r"-, "
            lastAddedWasDoubleBar = False
        elif value == HIDDEN_SIGN:
            # print("Adding Hidden Sign Detected..")
            fpxValuesTkz += r"h, "
            lastAddedWasDoubleBar = False
        elif value == MINUS_INFINITY:
            # print("Adding Minus Infinity Detected..")
            fpxValuesTkz += rf"-\infty, "   # BREAKING : NO DOLLAR SIGN AROUND -\infty
            lastAddedWasDoubleBar = False
        elif value == PLUS_INFINITY:
            # print("Adding Plus Infinity Detected..")
            fpxValuesTkz += rf"+\infty, "   # BREAKING : NO DOLLAR SIGN AROUND +\infty
            lastAddedWasDoubleBar = False
        # elif value == DOUBLE_BAR and not lastAddedWasDoubleBar:
        elif value == DOUBLE_BAR:
            if not lastAddedWasDoubleBar:
                print("Adding Double Bar Detected..")
                fpxValuesTkz += r"d, "
                lastAddedWasDoubleBar = True
        else:
            print("Adding Other Value Detected..=", value)
            fpxValuesTkz += rf"{get_latex(value)}, "
            lastAddedWasDoubleBar = False
    fpxValuesTkz = fpxValuesTkz.rstrip(" ")[:-1]    # rstrip last space and comma
    fpxValuesTkz = rf"""{{{fpxValuesTkz}}}"""
    return fpxValuesTkz

################################################
##                  FX VALUES
################################################

# def get_fxValues_from(tabvar):
def get_fxValues_from(fx:str, x:str):
    tabvar = get_tabvar(fx, x)
    l = tabvar[FX][1:]
    print("Initial l=", l)
    if est_fpaire(fx, x):
        print("Fonction Paire detected")
        l = extend_fxPaireValues(l)
    elif est_fimpaire(fx, x):
        print("Fonction Impaire detected")
        l = extend_fxImpaireValues(l)
    # set Fx Special Values in l1
    i = 0
    l1 = []
    xValues = get_xValues_from(fx, x)
    while i<=len(l)-3:
        if l[i] == UP_ARROW and l[i+2] == UP_ARROW:
            l1.append([xValues[int(i//2)+1], simplify(rf"{l[i+1]}"), i])
            l2 = l[:i+1]
            l2.append(None)
            l2.extend(l[i+3:])
            l = l2
        if l[i] == DOWN_ARROW and l[i+2] == DOWN_ARROW:
            l1.append([xValues[int(i//2)+1], simplify(rf"{l[i+1]}"), i])
            l2 = l[:i+1]
            l2.append(None)
            l2.extend(l[i+3:])
            l = l2
        print("AFTER l=", l)
        i += 1
    return l, l1

def get_fxValuesListOfTuples(fxValues):
    listOfTuples = []
    for i in range(len(fxValues)):
        if fxValues[i] == UP_ARROW:
            listOfTuples.append((fxValues[i-1], fxValues[i], fxValues[i+1]))
        elif fxValues[i] == DOWN_ARROW:
            listOfTuples.append((fxValues[i-1], fxValues[i], fxValues[i+1]))
    return listOfTuples

def get_fxValuesBy(triple):
    center = 1
    s = r""
    if triple[center] == UP_ARROW:
        s += rf"- / {triple[0]}, + / {triple[2]}"
    elif triple[center] == DOWN_ARROW:
        s += rf"+ / {triple[0]}, - / {triple[2]}"
    return s

def get_variation_indexes(fxValues):
    l = []
    for i in range(len(fxValues)):
        if fxValues[i] == UP_ARROW:
            l.append(i)
        elif fxValues[i] == DOWN_ARROW:
            l.append(i)
    return l 

def get_fxValuesTkz(fxValues:list)->str:
    """prepares fxValues for insertion into TkzTabVar VarTable
    """
    # i_varList = get_variation_indexes(fxValues)
    fxValuesTkz = r""
    # lastAddedWasDoubleBar = False
    last = len(fxValues) - 1
    i = 0
    # print("fxValues INSIDE=", fxValues)
    lastAdded = None
    while i <= last:
        print("i=", i)
        print("fxValuesTkz=", fxValuesTkz)
        if i <= last-1:
            if fxValues[i] == UP_ARROW:
                lastAdded = UP_ARROW
                print("UP ARROW DETECTED", fxValues[i])
                print("NEXT ONE DETECTED=", fxValues[i+1])
                # print("test ? ", fxValues[i+1] == "None")
                fxValuesTkz += rf"- / ${get_latex(fxValues[i-1])}$, "
                if fxValues[i+1] == "None":
                    # print("OK")
                    # count the number of Nones
                    # Add the correpsponding 'R/, '
                    # fxValuesTkz += rf"R /, + / ${get_latex(fxValues[i+2])}$, "
                    fxValuesTkz += rf"R /, "
                    i += 1
            if fxValues[i] == DOWN_ARROW:
                lastAdded = DOWN_ARROW
                fxValuesTkz += rf"+ / ${get_latex(fxValues[i-1])}$, "
                if fxValues[i+1] == "None":
                    # fxValuesTkz += rf"R /, - / ${get_latex(fxValues[i+2])}$, "
                    fxValuesTkz += rf"R /, "
                    i += 1
        if i == last:
            print("I=LAST")
            print("LastAdded=", lastAdded)
            # if fxValues[i] == UP_ARROW:
            if lastAdded == UP_ARROW:
                fxValuesTkz += rf"+ / ${get_latex(fxValues[i])}$, "
            # if fxValues[i] == DOWN_ARROW:
            if lastAdded == DOWN_ARROW:
                fxValuesTkz += rf"- / ${get_latex(fxValues[i])}$, "
        # print("i=", i)
        # if i == last:
        #     if lastVar == UP_ARROW:
        #         # fxValuesTkz += rf"- /${get_latex(fxValues[i-2])}$, + / ${get_latex(fxValues[i])}$, "
        #         fxValuesTkz += rf"- / ${get_latex(fxValues[i])}$, "
        #     else: # lastVar = DOWN_ARROW
        #         # fxValuesTkz += rf"+/ ${get_latex(fxValues[i-2])}$, - / ${get_latex(fxValues[i])}$, "
        #         fxValuesTkz += rf"+ / ${get_latex(fxValues[i])}$, "
        # if fxValues[i] == UP_ARROW:
        #     lastVar = UP_ARROW
        #     # if i+1 != last:
        #     if fxValues[i+1] != "+infinity":
        #         fxValuesTkz += rf"- / ${get_latex(fxValues[i-1])}$, + / ${get_latex(fxValues[i+1])}$, "
        #         i += 2
        #     elif fxValues[i+1] == "+infinity":
        #         if i+1 != last: # get Limit after Discontinuity
        #             fxValuesTkz += rf"- / ${get_latex(fxValues[i-1])}$, +D- / ${get_latex(fxValues[i+1])}$ / ${get_latex(fxValues[i+2])}$, "
        #             i += 3
        #         else: # i+1 = last
        #             # fxValuesTkz += rf"- / ${get_latex(fxValues[i-1])}$, +D / ${get_latex(fxValues[i+1])}$, "
        #             fxValuesTkz += rf"- / ${get_latex(fxValues[i-1])}$, + / ${get_latex(fxValues[i+1])}$, "
        # if fxValues[i] == DOWN_ARROW:
        #     lastVar = DOWN_ARROW
        #     # if i+1 != last:
        #     if fxValues[i+1] != "-infinity":
        #         fxValuesTkz += rf"+ / ${get_latex(fxValues[i-1])}$, - / ${get_latex(fxValues[i+1])}$, "
        #         i += 2
        #     elif fxValues[i+1] == "-infinity":
        #         if i+1 != last: # get Limit after discontinuity
        #             fxValuesTkz += rf"+ / ${get_latex(fxValues[i-1])}$, -D+ / ${get_latex(fxValues[i+1])}$ / ${get_latex(fxValues[i+2])}$, "
        #             i += 3
        #         else: # i+1 = last
        #             # fxValuesTkz += rf"+ / ${get_latex(fxValues[i-1])}$, -D / ${get_latex(fxValues[i+1])}$, "
        #             fxValuesTkz += rf"+ / ${get_latex(fxValues[i-1])}$, - / ${get_latex(fxValues[i+1])}$, "
        i += 1
    fxValuesTkz = fxValuesTkz.rstrip(" ")[:-1]    # rstrip last space and comma
    fxValuesTkz = rf"""{{{fxValuesTkz}}}"""
    return fxValuesTkz

def getFirstArrow(l):
    for i in range(len(l)):
        if l[i] == UP_ARROW:
            return i, UP_ARROW
        elif l[i] == DOWN_ARROW:
            return i, DOWN_ARROW
    return -1, "NOARROW"

def split_interval(interval):
    I = interval
    print("Interval I1=", I)
    i_eq = I.index("=") if "=" in I else -1
    if i_eq >= 0:
        I = I[I.index("=")+1:]
    # i_comma = I.index(",") if "=" in I else -1
    # if i_comma >= 0:
    #     I.replace(",", ";")
    openLeft = True if I[0] == "]" else False
    openRight = True if I[-1] == "[" else False
    I = I[1:-1]
    print("I2=", I)
    i_sep = I.index(";")
    a = I[:i_sep]
    b = I[i_sep+1:]
    # a = g.latex(a)
    # b = g.latex(b)
    print("openLeft=", openLeft)
    print("a=", a)
    print("b=", b)
    print("openRight=", openRight)
    return openLeft, a, b, openRight

if __name__ == "__main__":
    fprime = get_derivee("x^2")
    print("Hey")

