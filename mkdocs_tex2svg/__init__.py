"""
mkdocs_tex2svg library.
"""

from .mkdocs_tex2svg import *

__version__ = "0.1.1"
__author__ = 'Rodrigo Schwencke'
__credits__ = 'Rodrigo Schwencke'

